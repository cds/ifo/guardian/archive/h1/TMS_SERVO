
import time
import cdsutils as cdu
import ISC_library

from guardian import GuardState, GuardStateDecorator, NodeManager

nominal = 'TMS_SERVO_ON'

class INIT(GuardState):
    def run(self):
        return 'TMS_SERVO_OFF'


class TMS_SERVO_OFF(GuardState):
    index = 2
    request = True
    def run(self):
    
        return True


class TMS_SERVO_ON(GuardState):
    index = 5
    request = True

    @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def main(self):
        #check if POP PZT is railed
        if not ((abs(ezca['ASC-POP_X_PZT_PIT_OUTMON'])==ezca['ASC-POP_X_PZT_PIT_LIMIT'])
            or  (abs(ezca['ASC-POP_X_PZT_YAW_OUTMON'])==ezca['ASC-POP_X_PZT_YAW_LIMIT'])):
            self.POP_PZT_railed =False
        else:
            self.POP_PZT_railed = True

        # Set up TMS QPD servos to center beam on B QPDs by moving TMS
        self.xys = ['X', 'Y']
        self.pityaws = ['PIT', 'YAW']
        servo_gain = 0.1 # this works for all servos, converge in ~ 1 minute

        self.servos = {}
        for xy in self.xys:
            self.servos[xy] = {}
            for pityaw in self.pityaws:
                py = pityaw[0] # set py = 'P' or 'Y'

                ###   Set up servos   ###
                test_FM = ezca.get_LIGOFilter('SUS-TMS{xy}_M1_TEST_{py}'.format(xy=xy, py=py))

                # if OFFSET switch off, set offset to zero and turn it on
                if not test_FM.is_offset_on():
                    test_FM.OFFSET.put(0)
                    time.sleep(0.1)
                    test_FM.turn_on('OFFSET')

                test_FM.TRAMP.put(0) # set TRAMP to 0 because servo should be immediate

                # Create the servo on the TMS test filter offset controlled by its A QPD 
                # switched B QPD for A QPD on Jan 27, 2023, since B QPD is untrustworthy
                control_chan = 'SUS-TMS{xy}_M1_TEST_{py}_OFFSET'.format(xy=xy, py=py)
                readback_chan = 'ASC-{xy}_TR_B_{pityaw}_OUTMON'.format(xy=xy, pityaw=pityaw)
                servo = cdu.Servo(ezca, control_chan, readback=readback_chan, gain=servo_gain)

                self.servos[xy][pityaw] = servo # store the servo in a dictionary


    @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def run(self):
        #check that we haven't railed the POP PZT.  This will give a warning if it has railed.  We have seen the POP PZT be railed as the soft loops come on, because the alignment of PR3 was wrong.  We recovered from this situation by turning the soft loop inputs off, and walking PR3 back to where it used to be according to the top mass sliders.  If the loops have already been engaged when the PZT rails, this code will only notify you of the problem, and prevent you from moving on, it will not attempt to fix the problem.
        if not ((abs(ezca['ASC-POP_X_PZT_PIT_OUTMON'])==ezca['ASC-POP_X_PZT_PIT_LIMIT'])
            or  (abs(ezca['ASC-POP_X_PZT_YAW_OUTMON'])==ezca['ASC-POP_X_PZT_YAW_LIMIT'])):
            self.POP_PZT_railed =False
        else:
            self.POP_PZT_railed = True
        if self.POP_PZT_railed:
            notify('POP X PZT railed!!')

        # Execute our TMS QPD servos
        for xy in self.xys:
            for pityaw in self.pityaws:
                self.servos[xy][pityaw].step()

        return True
        
        
edges = [('TMS_SERVO_OFF', 'TMS_SERVO_ON'),
            ('TMS_SERVO_ON', 'TMS_SERVO_OFF'),]
